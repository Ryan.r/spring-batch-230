package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="category")
public class CategoryModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Category_Id;
	
	@Column(name="name")
	private String Name;

	public Long getCategory_Id() {
		return Category_Id;
	}

	public void setCategory_Id(Long category_Id) {
		Category_Id = category_Id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}
	
	public CategoryModel() {
	}
	
	public CategoryModel(String Name) {
		this.Name = Name;
	}
}
