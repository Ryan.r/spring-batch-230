package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Table(name="products")
public class ProductModel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Id;
	
	@Column(name="name")
	private String Name;
	
	@Column(name="price")
	private double Price;
	
	@Column(name="stock")
	private Integer Stock;
	
	@Column(name="category_id")
	private Integer Category_Id;

	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	private CategoryModel cm;
	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String name) {
		Name = name;
	}

	public double getPrice() {
		return Price;
	}

	public void setPrice(double price) {
		Price = price;
	}

	public Integer getStock() {
		return Stock;
	}

	public void setStock(Integer stock) {
		Stock = stock;
	}

	public Integer getCategory_Id() {
		return Category_Id;
	}

	public void setCategory_Id(Integer category_Id) {
		Category_Id = category_Id;
	}
}
