package xsis.latihan.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="variant")
public class VariantModel {
	
	@javax.persistence.Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long Variant_Id;
	
	@Column(name="variant_name")
	private String Variant_Name;
	
	@Column(name="category_id")
	private Long Category_Id;
	
	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	private CategoryModel cm;


	public Long getVariant_Id() {
		return Variant_Id;
	}

	public void setVariant_Id(Long variant_Id) {
		Variant_Id = variant_Id;
	}
	public CategoryModel getCm() {
		return cm;
	}
	
	public void setCm(CategoryModel cm) {
		this.cm = cm;
	}

	public String getVariant_Name() {
		return Variant_Name;
	}

	public void setVariant_Name(String variant_Name) {
		Variant_Name = variant_Name;
	}

	public Long getCategory_Id() {
		return Category_Id;
	}

	public void setCategory_Id(Long category_Id) {
		Category_Id = category_Id;
	}
	
	
}
