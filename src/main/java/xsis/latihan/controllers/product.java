package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.models.ProductModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.repositories.ProductRepo;

@Controller
@RequestMapping(value="/products/")
public class product {

	@Autowired
	private ProductRepo productrepo;
	
	@Autowired
	private CategoryRepo cr;
	
	@GetMapping(value="230")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/products/products");
		List<ProductModel> product = this.productrepo.findAll();
		mv.addObject("products", product);
		return mv;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView mv = new ModelAndView("/products/form");
		List<CategoryModel> cm = this.cr.findAll();
		ProductModel pr = new ProductModel();
		mv.addObject("products", pr);
		mv.addObject("category",cm);
		return mv;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute ProductModel pr, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:/products/form");
		} else {
			this.productrepo.save(pr);
			return new ModelAndView("redirect:/products/230");
		}
	}
	
	@GetMapping(value="edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/products/form");
		ProductModel pr = this.productrepo.findById(id).orElse(null);
		mv.addObject("products", pr);
		return mv;
	}
	
	@GetMapping(value="delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/products/delete");
		ProductModel pr = this.productrepo.findById(id).orElse(null);
		mv.addObject("products", pr);
		return mv;
	}
	
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute ProductModel pr, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:/products/form");
		} else {
			this.productrepo.delete(pr);
			return new ModelAndView("redirect:/products/230");
		}
	}
}
