package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.repositories.CategoryRepo;

@Controller
@RequestMapping(value="/category/")
public class Category {

	@Autowired
	private CategoryRepo cr;
	
	@GetMapping(value="cat")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/category/category");
		List<CategoryModel> cm = this.cr.findAll();
		mv.addObject("category", cm);
		return mv;
	}
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView mv = new ModelAndView("/category/form");
		CategoryModel cm = new CategoryModel();
		mv.addObject("category", cm);
		return mv;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute CategoryModel cm, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:/category/form");
		} else {
			this.cr.save(cm);
			return new ModelAndView("redirect:/category/cat");
		}
	}
	
	@GetMapping(value="edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/category/form");
		CategoryModel cm = this.cr.findById(id).orElse(null);
		mv.addObject("category", cm);
		return mv;
	}
	
	@GetMapping(value="delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/category/delete");
		CategoryModel cm = this.cr.findById(id).orElse(null);
		mv.addObject("category", cm);
		return mv;
	}
	
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute CategoryModel cm, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:/category/form");
		} else {
			this.cr.delete(cm);
			return new ModelAndView("redirect:/category/cat");
		}
	}
}
