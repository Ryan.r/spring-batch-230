package xsis.latihan.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.models.VariantModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.repositories.VariantRepo;

@Controller
@RequestMapping(value="/variant/")
public class Variant {

	@Autowired
	private VariantRepo vr;
	
	@GetMapping(value="var")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/variant/variant");
		List<VariantModel> vm = this.vr.findAll();
		mv.addObject("variant", vm);
		return mv;
	}
	
	@Autowired
	private CategoryRepo cr;
	
	@GetMapping(value="form")
	public ModelAndView form() {
		ModelAndView mv = new ModelAndView("/variant/form");
		List<CategoryModel> cm = this.cr.findAll();
		VariantModel vm = new VariantModel();
		mv.addObject("variant", vm);
		mv.addObject("category", cm);
		return mv;
	}
	
	@PostMapping(value="save")
	public ModelAndView save(@ModelAttribute VariantModel vm, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:/variant/form");
		} else {
			this.vr.save(vm);
			return new ModelAndView("redirect:/variant/var");
		}
	}
	
	@GetMapping(value="edit/{id}")
	public ModelAndView edit(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/variant/form");
		VariantModel vm = this.vr.findById(id).orElse(null);
		mv.addObject("variant", vm);
		return mv;
	}
	
	@GetMapping(value="delete/{id}")
	public ModelAndView delete(@PathVariable("id") Long id) {
		ModelAndView mv = new ModelAndView("/variant/delete");
		VariantModel vm = this.vr.findById(id).orElse(null);
		mv.addObject("variant", vm);
		return mv;
	}
	
	@PostMapping(value="remove")
	public ModelAndView remove(@ModelAttribute VariantModel vm, BindingResult br) {
		if(br.hasErrors()) {
			return new ModelAndView("redirect:/variant/form");
		} else {
			this.vr.delete(vm);
			return new ModelAndView("redirect:/variant/var");
		}
	}
}
