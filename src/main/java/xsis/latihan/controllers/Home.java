package xsis.latihan.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(value="/")
public class Home {

	@GetMapping(value="/")
	public ModelAndView index() {
		ModelAndView mv = new ModelAndView("/Home");
		return mv;
	}
}
