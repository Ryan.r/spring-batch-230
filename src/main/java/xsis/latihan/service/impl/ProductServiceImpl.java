package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.models.ProductModel;
import xsis.latihan.repositories.ProductRepo;
import xsis.latihan.service.ProductService;

@Service
public class ProductServiceImpl implements ProductService{

	@Autowired
	private ProductRepo pr;
	
	@Override
	public List<ProductModel> findAllProducts(){
		return pr.findAll();
	}
	
	@Override
	public ProductModel save(ProductModel products) {
		return pr.save(products);
	}
	
	@Override
	public void delete(Long Id) {
		pr.deleteById(Id);
	}
}
