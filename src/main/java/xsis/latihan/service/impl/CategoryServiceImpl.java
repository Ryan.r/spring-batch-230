package xsis.latihan.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xsis.latihan.models.CategoryModel;
import xsis.latihan.repositories.CategoryRepo;
import xsis.latihan.service.CategoryService;


@Service
public class CategoryServiceImpl implements CategoryService{

	@Autowired
	private CategoryRepo cr;
	
	@Override
	public List<CategoryModel> findAllCategory() {
		// TODO Auto-generated method stub
		return cr.findAll();
	}

	@Override
	public CategoryModel save(CategoryModel category) {
		return cr.save(category);
	}
	
	@Override
	public void delete(Long Category_Id) {
		cr.deleteById(Category_Id);
	}
	
}
