package xsis.latihan.service;

import java.util.List;

import xsis.latihan.models.ProductModel;

public interface ProductService {

	List<ProductModel> findAllProducts();
	
	ProductModel save(ProductModel products);
	
	void delete(Long Id);
}
