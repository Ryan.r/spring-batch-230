package xsis.latihan.restcontrollers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import xsis.latihan.models.ProductModel;
import xsis.latihan.service.ProductService;

@RestController
@RequestMapping(path="/api/products", produces="application/json")
@CrossOrigin(origins="*")
public class ProduckRestController {

	@Autowired
	private ProductService ps;
	
	@GetMapping("")
	public ResponseEntity<?> findAllProducts(){
		return new ResponseEntity<>(ps.findAllProducts(), HttpStatus.OK);
	}
	
	@PostMapping("/add")
	public ResponseEntity<?> saveProducts(@RequestBody ProductModel products){
		return new ResponseEntity<>(ps.save(products), HttpStatus.OK);
	}
	
	@PutMapping("/edit")
	public ResponseEntity<?> putProducts(@RequestBody ProductModel products){
		return new ResponseEntity<>(ps.save(products), HttpStatus.OK);
	}
	
	@DeleteMapping("/delete/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteProducts(@PathVariable("id") Long Id) {
		try {
			ps.delete(Id);
		} catch (EmptyResultDataAccessException e) {
			
		}
	}
}
